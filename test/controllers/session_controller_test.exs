defmodule Blogie.SessionControllerTest do
  use Blogie.ConnCase

  @valid_attrs %{username: "username", password: "password"}
  @invalid_attrs %{username: "differentusername", password: "differentpassword"}

  test "Logins with a valid user", %{conn: conn} do
    insert_user(@valid_attrs)
    conn = post conn, session_path(conn, :create), session: @valid_attrs
    html_response(conn, 302)
  end

  test "Logins with a invalid user", %{conn: conn} do
    insert_user(@valid_attrs)
    conn = post conn, session_path(conn, :create), session: @invalid_attrs
    html_response(conn, 200)
  end


  test "Logouts and tries to access an admin page", %{conn: conn} do
    user = insert_user(@valid_attrs)
    conn = post conn, session_path(conn, :create, session: @valid_attrs)
    conn = delete conn, session_path(conn, :delete, user)
    html_response(conn, 302)
  end
end
