defmodule Blogie.PostControllerTest do
  use Blogie.ConnCase

  @valid_attrs %{title: "title", content: "some content", publish_now: true}
  @invalid_attrs %{published_on: "not a valid date"}

end
