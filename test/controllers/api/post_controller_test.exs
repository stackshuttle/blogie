defmodule Blogie.API.PostControllerTest do
  use Blogie.ConnCase

  @valid_attrs %{content: "some content", publish_now: true, title: "some content"}
  @invalid_attrs %{published_on: "not a date"}
  @valid_user_attrs %{username: "user", password: "password"}

  setup %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "application/vnd.api+json")
      |> put_req_header("content-type", "application/vnd.api+json")
    {:ok, conn: conn}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, api_post_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    post = insert_post(@valid_attrs, insert_user(@valid_user_attrs)) |> Repo.preload(:author)

    conn = get conn, api_post_path(conn, :show, post)
    assert json_response(conn, 200)["data"] == %{
      "attributes" => %{
        "title" => post.title,
        "content" => post.content,
        "published-on" => Ecto.DateTime.to_iso8601(post.published_on)
      },
      "id" => "#{post.id}",
      "type" => "post",
      "relationships" =>
        %{"author" => %{"data" => %{"id" => "#{post.author.id}", "type" => "user"}}}
    }
  end

  test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, api_post_path(conn, :show, -1)
    end
  end
end
