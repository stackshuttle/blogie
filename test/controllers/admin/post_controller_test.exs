defmodule Blogie.Admin.PostControllerTest do
  use Blogie.ConnCase
  alias Blogie.Post

  @valid_attrs %{title: "title", content: "some content", publish_now: true}
  @valid_attrs_two %{title: "title2", content: "some content2", publish_now: true}
  @invalid_attrs %{published_on: "not a valid date"}

  setup %{ conn: conn} = config do
    if config[:login_as] do
      user = insert_user(%{username: config[:login_as], password: "password"})
      conn = assign(build_conn(), :current_user, user)
      {:ok, conn: conn, user: user}
    else
      :ok
    end
  end

  @tag login_as: "user"
  test "creates a new post and renders it", %{conn: conn, user: _user} do
    conn = post conn, admin_post_path(conn, :create), post: @valid_attrs
    html_response(conn, 302)
  end

  @tag login_as: "user"
  test "creates a new post with invalid parameters and check that it fails", %{conn: conn, user: _user} do
    conn = post conn, admin_post_path(conn, :create), post: @invalid_attrs
    html_response(conn, 200)
  end

  @tag login_as: "user"
  test "updates an existing post", %{conn: conn, user: user} do
    post = insert_post(@valid_attrs, user)
    conn = put conn, admin_post_path(conn, :update, post), post: @valid_attrs
    html_response(conn, 302)
  end

  @tag login_as: "user"
  test "updates an existing post with invalid parameters", %{conn: conn, user: user} do
    post = insert_post(@valid_attrs, user)
    conn = put conn, admin_post_path(conn, :update, post), post: @invalid_attrs
    html_response(conn, 200)
  end

  @tag login_as: "user"
  test "lists all posts", %{conn: conn, user: user} do
    insert_post(@valid_attrs, user)
    insert_post(@valid_attrs_two, user)
    conn = get conn, admin_post_path(conn, :index)
    articles = Regex.scan(~r/<article/, html_response(conn, 200))
    assert length(articles) == 2
  end

  @tag login_as: "user"
  test "renders a post", %{conn: conn, user: user} do
    post = insert_post(@valid_attrs, user)
    conn = get conn, admin_post_path(conn, :show, post)
    assert html_response(conn, 200) =~ @valid_attrs.content
  end

  @tag login_as: "user"
  test "deletes a post", %{conn: conn, user: user} do
    post = insert_post(@valid_attrs, user)
    conn = delete conn, admin_post_path(conn, :delete, post)
    assert redirected_to(conn) == admin_post_path(conn, :index)
    posts = Repo.all(Post)
    assert posts == []
  end
end
