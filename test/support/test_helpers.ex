defmodule Blogie.TestHelpers do
  alias Blogie.Repo

  def insert_post(attrs \\ %{}, user) do
    user
    |> Ecto.build_assoc(:posts)
    |> Blogie.Post.changeset(attrs)
    |> Repo.insert!()
  end

  def insert_user(attrs \\ %{}) do
    %Blogie.User{}
    |> Blogie.User.changeset(attrs)
    |> Repo.insert!()
  end
end

