defmodule Blogie.UserTest do
  use Blogie.ModelCase

  alias Blogie.User

  @valid_attrs %{username: "username", password: "some password", email: "bla@bla.com"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "password_changeset with valid attributes" do
    changeset = User.password_changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "password_changeset with invalid attributes" do
    changeset = User.password_changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end
end
