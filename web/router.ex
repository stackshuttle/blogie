defmodule Blogie.Router do
  use Blogie.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Blogie.AuthController, repo: Blogie.Repo
  end

  pipeline :api do
    plug :accepts, ["json-api"]
    plug JaSerializer.ContentTypeNegotiation
    plug JaSerializer.Deserializer
  end

  scope "/", Blogie do
    pipe_through :browser # Use the default browser stack

    get "/", PostController, :index
    get "/posts/:id", PostController, :show
    get "/about", AboutController, :show
    resources "/session", SessionController, only: [:new, :create, :delete]
  end

  scope "/admin", Blogie.Admin, as: :admin do
    pipe_through [:browser, :authenticate_user]

    resources "/", PostController
  end

  # Other scopes may use custom stacks.
  scope "/api", Blogie.API, as: :api do
    pipe_through :api

    resources "/posts", PostController, only: [:index, :show]
  end
end
