import React, {Component}from 'react';
import {render} from 'react-dom';
import ReactMarkdown from 'react-markdown';


class BlogContent extends Component {
  constructor(props) {
    super(props);

    const blogText = document.querySelector('#blog-content-hidden').innerText;
    this.state = {
      text: blogText
    };
  }

  onInputChange(text) {
     this.setState({text});
  }
  render() {

    return (
      <div>
        <label htmlFor="post_content">Content</label>
        <textarea
          onChange={event => this.onInputChange(event.target.value)}
          value={this.state.text}
          id="post_content" name="post[content]" rows="10">
        </textarea>

        <div className="preview">
          <h2 className="title">Preview</h2>
          <article id="markdown-target" className="type-system-serif">
          </article>
          <ReactMarkdown source={this.state.text} />
        </div>
      </div>
    );
  }
}

render(<BlogContent />, document.querySelector('#blog-content'));
