import React, {Component}from 'react';
import {render} from 'react-dom';
import ReactMarkdown from 'react-markdown';

class BlogContent extends Component {
  render() {
    const content = JSON.parse(document.querySelector('#markdown-source').innerHTML);

    return (
      <article key={content.title} className="markdown-target type-system-serif">
        <h1> {content.title}</h1>
        <h3>Author: {content.author.username}</h3>
        <ReactMarkdown source={content.content} />
      </article>
    )
  }
}


render(<BlogContent />, document.querySelector('#rendered-post'));