import React, {Component}from 'react';
import {render} from 'react-dom';
import ReactMarkdown from 'react-markdown';

const aboutText = document.querySelector('#markdown-source').innerText;
render(<ReactMarkdown source={aboutText} /> , document.querySelector('#rendered-about'));