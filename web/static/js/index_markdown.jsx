import React, {Component}from 'react';
import {render} from 'react-dom';
import ReactMarkdown from 'react-markdown';

class BlogContents extends Component {
  render() {
    const contents = JSON.parse(document.querySelector('#markdown-source').innerHTML);

    const contentNodes = contents.map((content) => {
      return (
        <article key={content.title} className="markdown-target type-system-serif">
          <h1> {content.title}</h1>
          <h3>Author: {content.author.username}</h3>
          <ReactMarkdown source={content.content} />
          <a href={content.url}>Read more</a>
        </article>
      )
    })
    return (
      <div>
        {contentNodes}
      </div>
    )
  }
}


render(<BlogContents />, document.querySelector('#rendered-posts'));
