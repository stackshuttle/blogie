defmodule Blogie.AboutController do
  use Blogie.Web, :controller

  def show(conn, _params) do
    render(conn, "show.html")
  end
end
