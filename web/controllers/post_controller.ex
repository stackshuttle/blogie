defmodule Blogie.PostController do
  use Blogie.Web, :controller

  alias Blogie.Post

  def show(conn, %{"id" => slug}) do
    query = from post in Post,
            where: post.published_on <= ^Ecto.DateTime.utc()
    post = Repo.get_by!(query, slug: slug)
           |> Repo.preload(:author)
    post_json = %{
      id: post.id,
      title: post.title,
      content: post.content,
      published_on: post.published_on,
      author: %{
        id: post.author.id,
        username: post.author.username
      }
    } |> Poison.encode!
    render(conn, "show.html", post: post_json)
  end

  def index(conn, _params) do
    query = from post in Post,
            where: post.published_on <= ^Ecto.DateTime.utc(),
            order_by: [desc: post.published_on]
    posts = Repo.all(query)
            |> Repo.preload(:author)
    posts_json = Enum.map(posts, fn post ->
      %{
        id: post.id,
        title: post.title,
        content: post.content,
        published_on: post.published_on,
        author: %{
          id: post.author.id,
          username: post.author.username
        },
        url: post_path(conn, :show, post)
    } end) |> Poison.encode!
    render(conn, "index.html", posts: posts_json)
  end
end
