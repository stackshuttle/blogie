defmodule Blogie.PageController do
  use Blogie.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
