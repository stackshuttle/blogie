defmodule Blogie.API.UserSerializer do
  use JaSerializer

  attributes [:username]
end
