defmodule Blogie.API.PostController do
  use Blogie.Web, :controller

  alias Blogie.Post

  def index(conn, _params) do
    posts = Repo.all(Post)
            |> Repo.preload(:author)
    render(conn, data: posts)
  end

  def show(conn, %{"id" => slug}) do
    post = Repo.get_by!(Post, slug: slug)
           |> Repo.preload(:author)
    render(conn, data: post)
  end

  def delete(conn, %{"id" => slug}) do
    post = Repo.get_by!(Post, slug: slug)
    Repo.delete!(post)
    send_resp(conn, :no_content, "")
  end
end
