defmodule Blogie.Admin.PostController do
  use Blogie.Web, :controller

  alias Blogie.Post

  plug :scrub_params, "post" when action in [:create, :update]

  def action(conn, _) do
    apply(__MODULE__, action_name(conn),
          [conn, conn.params, conn.assigns.current_user])
  end

  def new(conn, _, _) do
    changeset = Post.changeset(%Post{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"post" => post_params}, user) do
    changeset = user
                |> build_assoc(:posts)
                |> Post.changeset(post_params)

    case Repo.insert(changeset) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post created successfully.")
        |> redirect(to: admin_post_path(conn, :show, post))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => slug}, user) do
    post = Repo.get_by!(user_posts(user), slug: slug)
           |> Repo.preload(:author)
    changeset = Post.changeset(post)
    render(conn, "edit.html", changeset: changeset, post: post)
  end

  def update(conn, %{"id" => slug, "post" => post_params}, _) do
    post = Repo.get_by!(Post, slug: slug)
           |> Repo.preload(:author)

    changeset = Post.changeset(post, post_params)

    case Repo.update(changeset) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post updated successfully.")
        |> redirect(to: admin_post_path(conn, :show, post))
      {:error, changeset} ->
        render(conn, "edit.html", changeset: changeset, post: post)
    end
  end

  def index(conn, _params, _) do
    posts = Repo.all(Post)
            |> Repo.preload(:author)
    render(conn, "index.html", posts: posts)
  end

  def show(conn, %{"id" => slug}, _) do
    post = Repo.get_by!(Post, slug: slug)
           |> Repo.preload(:author)
    render(conn, "show.html", post: post)
  end

  def delete(conn, %{"id" => slug}, _) do
    post = Repo.get_by!(Post, slug: slug)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(post)

    conn
    |> put_flash(:info, "Post deleted successfully.")
    |> redirect(to: admin_post_path(conn, :index))
  end

  defp user_posts(user) do
    assoc(user, :posts)
  end
end
