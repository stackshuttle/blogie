defmodule Blogie.Post do
  use Blogie.Web, :model

  @derive {Poison.Encoder, only: [:title, :content, :published_on]}

  schema "posts" do
    field :title, :string
    field :content, :string
    field :published_on, Ecto.DateTime
    field :publish_now, :boolean, virtual: true, default: false
    field :slug, :string
    belongs_to :author, Blogie.User

    timestamps
  end

  @required_fields ~w(title content)
  @optional_fields ~w(published_on publish_now)

  defp publish_post_if_necessary(changeset) do
    cond do
      changeset.changes[:publish_now] == true ->
        put_change(changeset, :published_on, Ecto.DateTime.utc())
      true ->
        put_change(changeset, :published_on, nil)
    end
  end

  defp slugify_title(changeset) do
    if title = get_change(changeset, :title) do
      put_change(changeset, :slug, Slugger.slugify_downcase(title))
    else
      changeset
    end
  end

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> slugify_title
    |> unique_constraint(:slug)
    |> publish_post_if_necessary
  end
end

defimpl Phoenix.Param, for: Blogie.Post do
  def to_param(%{slug: slug, id: _id}) do
    slug
  end
end
