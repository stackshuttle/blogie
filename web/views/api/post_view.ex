defmodule Blogie.API.PostView do
  use Blogie.Web, :view
  use JaSerializer.PhoenixView

  attributes [:title, :content, :published_on]
  has_one :author,
    serializer: Blogie.API.UserSerializer,
    include: true
end
