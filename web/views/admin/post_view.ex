defmodule Blogie.Admin.PostView do
  use Blogie.Web, :view

  # The first element of the tuple represents the css class
  # and the second the text of the state
  def published?(post) do
    case post.published_on do
      nil -> false
      _ -> true
    end
  end
end
