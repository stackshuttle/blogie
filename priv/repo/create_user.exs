alias Blogie.User
alias Blogie.Repo

username = IO.gets "Choose a username for your new user:\n"
password = IO.gets "Choose a password:\n"
email = IO.gets "Choose an email address:\n"

username = String.replace(username, "\n", "")
password = String.replace(password, "\n", "")
email = String.replace(email, "\n", "")

changeset = User.changeset(%User{}, %{username: username, password: password, email: email})
case Repo.insert(changeset) do
  {:ok, post} -> IO.puts "User created successfully"
  {:error, changeset} -> IO.inspect changeset.errors
end
