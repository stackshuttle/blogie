defmodule Blogie.Repo.Migrations.AddAuthorOnPost do
  use Ecto.Migration

  def change do
    alter table(:posts) do
      add :author_id, references(:users)
    end
    create index(:posts, [:author_id])
  end
end
