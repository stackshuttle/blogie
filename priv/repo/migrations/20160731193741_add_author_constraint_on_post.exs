defmodule Blogie.Repo.Migrations.AddAuthorConstraintOnPost do
  use Ecto.Migration

  def change do
    alter table(:posts) do
      modify :author_id, :integer, [null: false]
    end
  end
end
