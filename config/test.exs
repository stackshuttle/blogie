use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :blogie, Blogie.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :blogie, Blogie.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRESQL_USER") || "postgres",
  password: System.get_env("POSTGRESQL_PASSWORD") || "postgres",
  database: "blogie_test",
  hostname: System.get_env("POSTGRESQL_HOST") || "localhost",
  port: System.get_env("POSTGRESQL_PORT") || 5432,
  pool: Ecto.Adapters.SQL.Sandbox
