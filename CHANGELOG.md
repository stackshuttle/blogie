# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.3]
### Changed
- Updated `phoenix` version to 1.2.0
- Updated `comeonin` version to 2.5.1
- Updated `db_connection` version to 1.0-rc4
- Updated `phoenix_ecto` version to 3.0
- Updated `ja_serializer` version to 0.10.1
- Updated docker image to use Elixir 1.3.1
- Removed some warnings

## [0.1.2]
### Changed
- Fixed a CSS issue on published posts

## [0.1.1]
### Added
- New nginx configuration for http only
- Fixed webpack configuration for deployment


## [0.1] - 2016-05-17
## Added
- CRUD of posts if you are a logged in users
- Read individual or all posts for all users
