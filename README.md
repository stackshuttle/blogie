# Blogie

Blogie is a blog application. It is a multi user blog, meaning that posts are owned by users.
Users manage the content:
  * CRUD of posts
  * Making a post published or draft

Non authenticated users can read all published posts.

Users are created using commands on the server (See [Blogie commands](#blogie-commands) section).

To start your Blogie app:

  * Install dependencies with `mix deps.get`
  * If you are using custom PostgreSQL user (default being postgres),
    add `EXPORT POSTGRESQL_USER=$USER` in your `~/.bashrc` or `~/.zshrc`
    (do not forget to source the file after editing with `source ~/.bashrc`)

    For MacOS users: add this to your `~/.bash_profile` (create the file if it doesn't exist)

  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `npm install`
  * Create a user with `mix run priv/repo/create_user.exs`
  * Run tests to make sure everything is ok with `mix test` (if any issue, contact us)
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Blogie commands

  * Create user: `mix run priv/repo/create_user.exs`
  * Change password: `mix run priv/repo/change_password.exs`

## Deployment

For deployment, a sample nginx configuration file is available at `config/blogie.nginx.conf`.
The file includes settings for `https`.
If you do not want them, simply remove the lines related to `https`,
it is indicated in the comments of the file.


On a new ubuntu server, you will need to install the following packages:

 1. Install system packages

        sudo aptitude update && sudo aptitude upgrade
        sudo aptitude install postgresql nginx make gcc git

 2. Install Erlang and elixir

        wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && sudo dpkg -i erlang-solutions_1.0_all.deb
        # If asked for distribution name, type: `wily`
        sudo aptitude update
        sudo aptitude install esl-erlang elixir

 3. Install nvm and node

        curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash
        source ~/.bashrc
        nvm install v5.9.1
        npm install

 3. Configure postgresql

        # Logs in as the postgres user
        sudo su postgres
        createuser -d -P <username> # replace <username> with your shell username
        exit
        # back as the initial user

 4. Clone repository

        git clone https://gitlab.com/stackshuttle/blogie.git
        cd blogie
        git checkout 0.1.3

 5. Install dependencies

        mix deps.get --only prod


Copy this content into `config/prod.secret.exs`.

    use Mix.Config

    # In this file, we keep production configuration that
    # you likely want to automate and keep it away from
    # your version control system.

    # You can generate a new secret by running:
    #
    #     mix phoenix.gen.secret
    config :blogie, Blogie.Endpoint,
      secret_key_base: "lHoyx8jUgRDmow9FgakKp5eCgTjmlLJmEiwpmln+4vTZfyO3yoBn1x4vdJ6Pwm6k"

Then, generate a new secret key with the command: `mix phoenix.gen.secret`.
Finally, replace the value of `secret_key_base` with the value generated.

In your `~/.bashrc` file, you will need to add two extra lines:

    export POSTGRESQL_USER=$USER
    export POSTGRESQL_PASSWORD=<yourpassword>

Then, don't forget to source your `~/.bashrc`

    source ~/.bashrc

The following commands will build your static files, compile everything, create and migrate
the database, create the digest file, import seed data and finally create an admin user.

    node_modules/webpack/bin/webpack.js
    MIX_ENV=prod mix compile
    MIX_ENV=prod mix ecto.create
    MIX_ENV=prod mix ecto.migrate
    MIX_ENV=prod mix phoenix.digest
    MIX_ENV=prod mix run priv/repo/create_user.exs # Creates a user
    MIX_ENV=prod mix run test  # makes sure everything works fine

Finally, you can test this configuration by running:

    PORT=4001 MIX_ENV=prod mix phoenix.server

If you can access the machine over the port 4001, you should be able to open it in your browser:
http://<ip of the machine>:4001/

The final step, is to configure nginx on port 80 to redirect internally to the port 4001.

Copy `config/blogie.nginx.conf` to `/etc/nginx/site-available/` (as root)
The nginx config file assumes you have generated a certificate for your app (https). In this example,
we use [letsencrypt](https://letsencrypt.org/getting-started/).
Make a symlink in `/etc/nginx/site-enabled/`, and remove the default file

    ln -s /etc/nginx/sites-available/blogie.nginx.conf /etc/nginx/sites-enabled/
    rm /etc/ngix/sites-enabled/default

Reload nginx configuration:

    service nginx reload

Now you can access your website on the port 80!


## Contact us

  * Join us on slack: http://ssoss-slack-invite.herokuapp.com/
  * Fork this repo: https://gitlab.com/stackshuttle/blogie/forks/new

## Learn More

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: http://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
